package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"rollingsoftware/gambler/core/gambles"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v4/pgxpool"
)

func indexHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", gin.H{
		"title": "Русский военный корабль, иди на х*й",
	})
}

func setupDatabase(ctx context.Context, pool *pgxpool.Pool) error {
	migrations := []string{
		`
		CREATE TABLE IF NOT EXISTS gambles (
			id       		SERIAL PRIMARY KEY,
			title           VARCHAR(100) NOT NULL,
			code            VARCHAR(36) NOT NULL,
			description     TEXT NOT NULL,
			created_at		TIMESTAMP NOT NULL,

			CONSTRAINT 		gambles_code_uidx UNIQUE (code)
		)
		`,
		`
		CREATE TABLE IF NOT EXISTS bets (
			id       		SERIAL PRIMARY KEY,
			gamble_id  		INT references gambles(id),
			username        VARCHAR(100) NOT NULL,
			code            VARCHAR(36) NOT NULL,
			amount          INT NOT NULL,
			place			INT NOT NULL,
			description     TEXT,
			updated_at		TIMESTAMP NOT NULL,
			created_at		TIMESTAMP NOT NULL,

			CONSTRAINT 		bets_code_uidx UNIQUE (code),
			CONSTRAINT 		bets_gamble_id_username_uidx UNIQUE (gamble_id, username)
		)
		`,
		`
		CREATE TABLE IF NOT EXISTS api_keys (
			code       		VARCHAR(36) PRIMARY KEY
		)
		`,
	}

	for _, sql := range migrations {
		_, err := pool.Exec(ctx, sql)
		if err != nil {
			return err
		}
	}
	return nil
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
		log.Println("PORT for web app is not set, using default value " + port)
	}

	dbUrl := os.Getenv("DATABASE_URL")
	if dbUrl == "" {
		dbUrl = "postgresql://root:test@localhost/gambler"
		log.Println("DATABASE_URL is not set, using default value " + dbUrl)
	}

	secret := os.Getenv("SECRET")
	if secret == "" {
		log.Fatalln("SECRET is not set, shutting down")
	}

	log.Println("Configuration acquired")

	ctx := context.Background()
	pool, err := pgxpool.Connect(ctx, dbUrl)
	if err != nil {
		log.Printf("Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	defer pool.Close()
	log.Println("Database connection established")

	err = setupDatabase(ctx, pool)
	if err != nil {
		log.Printf("Unable to setup database: %v\n", err)
		os.Exit(2)
	}

	r := gin.New()
	r.Use(gin.Logger())
	r.LoadHTMLGlob("templates/**/*")

	cookieStore := cookie.NewStore([]byte(secret))
	r.Use(sessions.Sessions("session", cookieStore))

	r.Use(gin.CustomRecovery(func(c *gin.Context, recovered interface{}) {
		c.HTML(http.StatusInternalServerError, "error.html", gin.H{
			"error": "Server error",
		})
	}))

	r.NoRoute(func(c *gin.Context) {
		c.HTML(http.StatusNotFound, "error.html", gin.H{
			"error": "Not found",
		})
	})

	r.GET("/", indexHandler)
	r.GET("/index", indexHandler)

	gambles.RegisterRoutes(r, ctx, pool)

	err = r.Run(":" + port)
	if err != nil {
		log.Printf("Failed to run: %v", err)
		return
	}
}
