# Gambler

## About
Primitive web app to create and manage gambles with bets

## Requirements
 * golang 1.18

## How to build and run
To build the project just the command
```bash
go build -o ./bin/gambler
```

To run you will need to setup a **postgres** instance

You can use provided **docker-compose.yml** to spin up a **postgres** instance for development purposes, just by running:
```bash
docker-compose up
```

And provide the application with the following **environment variables**:
```bash
PORT=8080
DATABASE_URL="postgresql://root:test@localhost/gambler"
SECRET="RANDOMLY_GENERATED_SECURE_SECRET"
```

To run the application during development you can simply use the following command:
```bash
SECRET=DEV go run main.go
```

For production use please use real secret, port and database credentials and run the built binary from bin directory:
```bash
./gambler
```