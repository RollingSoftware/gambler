package api

import (
	"context"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

func ApiKeyExists(ctx context.Context, pool *pgxpool.Pool, key uuid.UUID) bool {
	row := pool.QueryRow(ctx, "SELECT code FROM api_keys WHERE code = $1", key.String())

	var existingKey string
	err := row.Scan(&existingKey)
	if err != nil || existingKey == "" {
		return false
	}
	return true
}
