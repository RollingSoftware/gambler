package gambles

import (
	"context"
	"errors"
	"log"
	"net/http"
	"rollingsoftware/gambler/core/api"
	"strconv"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type gamble struct {
	id          int
	title       string
	code        uuid.UUID
	description string
	createdAt   time.Time
}

type Bettor struct {
	Username    string
	Amount      int
	Place       int
	Description string
	Code        string
}

type BettorView struct {
	Username    string
	Amount      int
	Place       int
	Description string
	CanEdit     bool
	GambleCode  string
}

func queryGambleByCode(ctx context.Context, pool *pgxpool.Pool, searchCode uuid.UUID) (*gamble, error) {
	row := pool.QueryRow(
		ctx,
		"SELECT id, title, code, description, created_at FROM gambles WHERE code = $1",
		searchCode.String(),
	)

	gamble := gamble{}
	err := row.Scan(&gamble.id, &gamble.title, &gamble.code, &gamble.description, &gamble.createdAt)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return &gamble, nil
}

func GetGambleHandler(c *gin.Context, ctx context.Context, pool *pgxpool.Pool, isEdit bool) {
	gambleCode := c.Param("gambleCode")
	if gambleCode == "" {
		invalidRequest(c)
		return
	}

	gambleCodeUuid, err := uuid.Parse(gambleCode)
	if err != nil {
		invalidRequest(c)
		return
	}

	gamble, err := queryGambleByCode(ctx, pool, gambleCodeUuid)
	if err != nil {
		invalidRequest(c)
		return
	}

	bettors, err := getBettors(ctx, pool, gambleCodeUuid)
	if err != nil {
		serverError(c)
		return
	}

	session := sessions.Default(c)
	bettorCode := session.Get("bettorCode")

	var bettorsView []BettorView
	for _, bettor := range bettors {
		bettorsView = append(
			bettorsView,
			BettorView{
				Username:    bettor.Username,
				Amount:      bettor.Amount,
				Place:       bettor.Place,
				Description: bettor.Description,
				CanEdit:     bettor.Code == bettorCode,
				GambleCode:  gambleCode,
			},
		)
	}

	c.HTML(http.StatusOK, "gamble.html", gin.H{
		"title":       gamble.title,
		"description": gamble.description,
		"gambleCode":  gamble.code.String(),
		"bettors":     bettorsView,
		"bettorCode":  bettorCode,
		"isEdit":      isEdit,
	})
}

func invalidRequest(c *gin.Context) {
	c.HTML(http.StatusBadRequest, "error.html", gin.H{
		"error": "Invalid request",
	})
}

func serverError(c *gin.Context) {
	c.HTML(http.StatusInternalServerError, "error.html", gin.H{
		"error": "Server error",
	})
}

func CreateGamblePageHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "create-gamble-page.html", gin.H{})
}

func CreateGambleHandler(c *gin.Context, ctx context.Context, pool *pgxpool.Pool) {
	code := c.PostForm("code")
	if code == "" {
		log.Println("Missing code")
		invalidRequest(c)
		return
	}

	codeUuid, err := uuid.Parse(code)
	if err != nil {
		log.Println("Cannot parse UUID")
		invalidRequest(c)
		return
	}

	keyFound := api.ApiKeyExists(ctx, pool, codeUuid)
	if !keyFound {
		log.Println("Could not find key")
		invalidRequest(c)
		return
	}

	title := c.PostForm("title")
	if title == "" {
		log.Println("Missing title")
		invalidRequest(c)
		return
	}

	description := c.PostForm("description")
	if description == "" {
		log.Println("Missing description")
		invalidRequest(c)
		return
	}

	newGambleCode := uuid.New()
	_, err = pool.Query(
		ctx,
		"INSERT INTO gambles (title, description, code, created_at) VALUES ($1, $2, $3, $4)",
		title, description, newGambleCode, time.Now(),
	)
	if err != nil {
		serverError(c)
		return
	}

	c.HTML(http.StatusOK, "code-gamble-page.html", gin.H{
		"code": newGambleCode.String(),
	})
}

func parseParamUuid(c *gin.Context, name string) (*uuid.UUID, error) {
	value := c.Param(name)
	if value == "" {
		return nil, errors.New(name + " is empty")
	}

	uuid, err := uuid.Parse(value)
	if err != nil {
		return nil, errors.New(value + " cannot parse as uuid")
	}
	return &uuid, nil
}

func UnBetHandler(c *gin.Context, ctx context.Context, pool *pgxpool.Pool) {
	gambleCodeUuid, err := parseParamUuid(c, "gambleCode")
	if err != nil {
		invalidRequest(c)
		return
	}

	session := sessions.Default(c)
	bettorCode := session.Get("bettorCode")
	if bettorCode == nil {
		invalidRequest(c)
		return
	}

	bettorCodeUuid, err := uuid.Parse(bettorCode.(string))
	if err != nil {
		log.Println("failed to parse bettor code to uuid")
		invalidRequest(c)
		return
	}

	rows, err := pool.Exec(
		ctx,
		`
		DELETE FROM bets WHERE gamble_id = (SELECT id FROM gambles WHERE code = $1) AND code = $2
		`,
		gambleCodeUuid.String(),
		bettorCodeUuid.String(),
	)
	if err != nil {
		log.Printf("Failed to update bet: %v", err)
		serverError(c)
		return
	}
	if rows.RowsAffected() != 1 {
		log.Printf("Affected rows is not one: %d", rows.RowsAffected())
		serverError(c)
		return
	}
	session.Delete("bettorCode")
	session.Save()
	c.Redirect(http.StatusFound, "/gamble/"+gambleCodeUuid.String())
}

func BetHandler(c *gin.Context, ctx context.Context, pool *pgxpool.Pool) {
	gambleCodeUuid, err := parseParamUuid(c, "gambleCode")
	if err != nil {
		invalidRequest(c)
		return
	}

	session := sessions.Default(c)
	bettorCode := session.Get("bettorCode")
	if bettorCode == nil {
		newBettorCode := uuid.New()
		err = createBet(c, ctx, pool, gambleCodeUuid, newBettorCode)
		if err != nil {
			invalidRequest(c)
			return
		}
		session.Set("bettorCode", newBettorCode.String())
		session.Save()
	} else {
		bettorCodeUuid, err := uuid.Parse(bettorCode.(string))
		if err != nil {
			log.Println("failed to parse bettor code to uuid")
			invalidRequest(c)
			return
		}

		err = updateBet(c, ctx, pool, gambleCodeUuid, bettorCodeUuid)
		if err != nil {
			invalidRequest(c)
			return
		}
	}
	c.Redirect(http.StatusFound, "/gamble/"+gambleCodeUuid.String())
}

type betCreateRequest struct {
	username    string
	amount      int
	place       int
	description string
}

type betUpdateRequest struct {
	amount      int
	place       int
	description string
}

func parseBetUpdateRequest(c *gin.Context) (betUpdateRequest, error) {
	amount := c.PostForm("amount")
	place := c.PostForm("place")
	description := c.PostForm("description")

	amountInt, err := strconv.Atoi(amount)
	if err != nil {
		return betUpdateRequest{}, errors.New("failed to parse amount")
	}

	if amountInt <= 0 {
		return betUpdateRequest{}, errors.New("invalid amount")
	}

	placeInt, err := strconv.Atoi(place)
	if err != nil {
		return betUpdateRequest{}, errors.New("failed to parse place")
	}

	if placeInt <= 0 {
		return betUpdateRequest{}, errors.New("invalid place")
	}

	return betUpdateRequest{amount: amountInt, place: placeInt, description: description}, nil
}

func parseBetRequest(c *gin.Context) (betCreateRequest, error) {
	username := c.PostForm("username")
	if username == "" {
		return betCreateRequest{}, errors.New("username is mandatory")
	}

	amount := c.PostForm("amount")
	place := c.PostForm("place")
	description := c.PostForm("description")

	amountInt, err := strconv.Atoi(amount)
	if err != nil {
		return betCreateRequest{}, errors.New("failed to parse amount")
	}

	if amountInt <= 0 {
		return betCreateRequest{}, errors.New("invalid amount")
	}

	placeInt, err := strconv.Atoi(place)
	if err != nil {
		return betCreateRequest{}, errors.New("failed to parse place")
	}

	if placeInt <= 0 {
		return betCreateRequest{}, errors.New("invalid place")
	}

	return betCreateRequest{username: username, amount: amountInt, place: placeInt, description: description}, nil
}

func createBet(c *gin.Context, ctx context.Context, pool *pgxpool.Pool, gambleCode *uuid.UUID, bettorCode uuid.UUID) error {
	betRequest, err := parseBetRequest(c)
	if err != nil {
		log.Printf("Failed to parse bet %v\n", err)
		return err
	}

	rows, err := pool.Exec(
		ctx,
		`
		INSERT INTO bets (gamble_id, username, code, amount, place, description, created_at, updated_at) 
		VALUES ((SELECT id FROM gambles WHERE code = $1), $2, $3, $4, $5, $6, $7, $8)
		`,
		gambleCode,
		betRequest.username,
		bettorCode,
		betRequest.amount,
		betRequest.place,
		betRequest.description,
		time.Now(),
		time.Now(),
	)
	if err != nil {
		log.Printf("Failed to insert bet data: %v\n", err)
		return err
	}
	if rows.RowsAffected() != 1 {
		return errors.New("failed to insert bet")
	}
	return nil
}

func updateBet(c *gin.Context, ctx context.Context, pool *pgxpool.Pool, gambleCode *uuid.UUID, bettorCode uuid.UUID) error {
	betRequest, err := parseBetUpdateRequest(c)
	if err != nil {
		return err
	}

	rows, err := pool.Exec(
		ctx,
		`
		UPDATE bets SET amount=$1, place=$2, description=$3, updated_at=$4
		WHERE gamble_id = (SELECT id FROM gambles WHERE code = $5) AND code = $6
		`,
		betRequest.amount,
		betRequest.place,
		betRequest.description,
		time.Now(),
		gambleCode,
		bettorCode,
	)
	if err != nil {
		log.Printf("Failed to update bet: %v", err)
		return err
	}
	if rows.RowsAffected() != 1 {
		log.Printf("Affected rows is not one: %d", rows.RowsAffected())
		return errors.New("failed to update bet")
	}
	return nil
}

func getBettors(ctx context.Context, pool *pgxpool.Pool, gambleCode uuid.UUID) ([]Bettor, error) {
	rows, err := pool.Query(
		ctx,
		`
		SELECT username, amount, place, description, code FROM bets
		WHERE gamble_id = (SELECT id FROM gambles WHERE code = $1)
		ORDER BY place ASC, amount DESC, username DESC
		`,
		gambleCode.String(),
	)
	if err != nil {
		log.Printf("Failed to query bettors %v\n", err)
		return nil, errors.New("failed to retrieve bettors")
	}

	var bettors []Bettor
	for rows.Next() {
		var bettor Bettor
		err = rows.Scan(&bettor.Username, &bettor.Amount, &bettor.Place, &bettor.Description, &bettor.Code)
		if err != nil {
			log.Printf("Failed to scan bettor %v\n", err)
			return nil, errors.New("failed to scan bettor")
		}
		bettors = append(bettors, bettor)
	}
	return bettors, nil
}

func RegisterRoutes(r *gin.Engine, ctx context.Context, pool *pgxpool.Pool) {
	r.POST("/gamble/create", func(c *gin.Context) { CreateGambleHandler(c, ctx, pool) })
	r.GET("/gamble/create", func(c *gin.Context) { CreateGamblePageHandler(c) })
	r.GET("/gamble/:gambleCode", func(c *gin.Context) { GetGambleHandler(c, ctx, pool, false) })
	r.GET("/gamble/:gambleCode/edit", func(c *gin.Context) { GetGambleHandler(c, ctx, pool, true) })
	r.POST("/gamble/:gambleCode/bet", func(c *gin.Context) { BetHandler(c, ctx, pool) })
	r.POST("/gamble/:gambleCode/unbet", func(c *gin.Context) { UnBetHandler(c, ctx, pool) })
}
